import atexit
import gettext
import os
from functools import lru_cache
from gettext import NullTranslations, GNUTranslations
from tempfile import TemporaryDirectory
from typing import Optional, Union, Set, Iterable, TYPE_CHECKING

from debputy import DEBPUTY_IS_RUN_FROM_SOURCE, DEBPUTY_ROOT_DIR
from debputy.util import _debug_log

try:
    import polib

    HAS_POLIB = True
except ImportError:
    HAS_POLIB = False

if TYPE_CHECKING:
    import polib

Translations = Union[NullTranslations, GNUTranslations]


def N_(v: str) -> str:
    return v


@lru_cache
def _temp_messages_dir() -> str:
    temp_dir = TemporaryDirectory(delete=False, ignore_cleanup_errors=True)
    atexit.register(lambda: temp_dir.cleanup())
    return temp_dir.name


_GENERATED_MO_FILES_FOR: Set[str] = set()


def translation(
    domain: str,
    *,
    languages: Optional[Iterable[str]] = None,
) -> Translations:
    if DEBPUTY_IS_RUN_FROM_SOURCE and HAS_POLIB:
        po_domain_dir = DEBPUTY_ROOT_DIR / "po" / domain
        locale_dir = _temp_messages_dir()
        if po_domain_dir.is_dir() and domain not in _GENERATED_MO_FILES_FOR:
            for child in po_domain_dir.iterdir():
                if not child.is_file() or not child.name.endswith(".po"):
                    continue
                language = child.name[:-3]
                mo_dir = os.path.join(locale_dir, language, "LC_MESSAGES")
                os.makedirs(mo_dir, exist_ok=True)
                mo_path = os.path.join(mo_dir, f"{domain}.mo")
                parsed_po_file = polib.pofile(child)
                parsed_po_file.save_as_mofile(mo_path)
            _GENERATED_MO_FILES_FOR.add(domain)
        try:
            r = gettext.translation(
                domain,
                localedir=locale_dir,
                languages=languages,
                fallback=True,
            )
            _debug_log(f"Found in-source translation for {domain}")
            return r
        except FileNotFoundError:
            # Fall through
            _debug_log(
                f"No in-source translation for {domain}, trying to installed translations"
            )

    # For some reason, the type checking thinks that `fallback` must be `Literal[False]` which
    # defeats the purpose of being able to provide a different value than the default for it.
    #
    # So `type: ignore` to "fix" that for now.
    return gettext.translation(
        domain,
        languages=languages,
        fallback=True,
    )


__all__ = ["N_", "translation", "Translations"]
