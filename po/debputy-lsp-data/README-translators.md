# Translatable strings for `debputy`'s LSP module

Thanks for contributing to the `debputy` Language Server Protocol (LSP) module. :)

This document is here to help you with some context of what you are working with.

# What is the LSP

The Language Server Protocol is a protocol between IDE editors and language
servers. The language servers will provide a set of features for a given
language or data format like resolving documentation, providing completion
suggestions, or emitting diagnostics (warnings and errors). The `debputy`
tool provides a language server that delivers features for Debian packaging
files.

The Language Server Protocol describes how the editors and language
servers communicate and thereby enable all LSP supporting editors to
leverage `debputy` to provides the language features.

# Target audience of the strings

The target audience of these strings is people who work on Debian packages, of any experience
level. All the strings are very technical by nature but at the same time should also be understandable
for someone very new to the packaging concepts.

Producing the initial strings involves striking a difficult balance, which may also be difficult
for you as a translator. If it comes to balancing the interests of newcomers and seasoned
packagers, the text and its translation should prioritize newcomers.

The project is written and maintained by seasoned packagers, which in may be a general weak point in the
documentation that the project provides in.

# Scope of the strings and how they are used

The strings in this GNU gettext domain cover known fields and, where possible, known values of those fields.
At this time the strings cover `debian/control`, `debian/copyright` (DEP-5) format and `debian/tests/control`.

The strings are designed to be used in an IDE editor, which will take care of rendering markup. As a
consequence, they are subject to the same rule against making assumptions about exact presentation
and rendering that applies to debconf templates. Each front-end may render them slightly differently.

There are two major types of strings:

 * [Synopsis](#synopsis)
 * [Hover Doc (`long-description`)](#hover-docs-long-description)

When and how they are used will be covered in their respective subsections.

## Synopsis

The `synopsis` is used with "completion" actions; that is, when
the editor is showing the user "here are the options for what you
can insert here". The following screenshot provides an example:

![Screenshot showing a completion suggestion with synopsis in emacs](lsp-synopsis-completion.png)

The `synopsis` is a one-line plain-text string. It will be rendered
in association with the value / field name it relates to. In this
and many other ways, it works a lot like the Synopsis of a Debian
package (and needs to squeeze a lot into a very limited space).

The protocol used has support for marking completions as obsolete,
where the editor has a way of showing this to the user (strike-through
is a common pattern). Therefore, this should never
be specified in the text itself as there is a code level flag for
that *if* there is anything better to say in the synopsis.
That said, there are a couple of cases where we do not know what
the field did. ake for example `XS-Ruby-Versions`, which has
`Obsolete (unknown usage)` as the synopsis for the lack of anything
better. Here, using "Obsolete" in the synopsis was an acceptable
trade-off compared to hunting down what the obsolete field did.

A few cases still use `**Advanced field**` (etc.) in their synopsis
to steer newcomers away from them or warn that they are
potential time-sinks. The plan is to extract these into a general
marker that is included in the synopsis as a prefix. This has
already been done with "rare (but not obsolete)" fields/values.

TODO:

 * Some "prefixes" should be turned into markers.
 * The markers themselves are currently not translatable.

## Hover docs (`long-description`)

The Hover docs (called `long-description` in the source file) are
used with LSP `Hover` requests. These are used when the user
requests more information from the editor about what they see.
The name "Hover" is used because this is usuallly
done by hovering the cursor over the subject of interest. The
following screenshot provides an example:

![Screenshot showing hover docs of a field in emacs](lsp-hover-doc.png)

The strings here are Markdown formatted strings that can span
multiple lines. The `debputy` side also auto-generates a header
with field name and, for values, the value. This header is
therefore omitted from the strings themselves. The header concrete
is one of:

  * For field names

    ```markdown
    # `{FIELD_NAME}`
    ```

  * For field values:

    ```markdown
    # `{VALUE}` (Field: `{FIELD_NAME}`)
    ```

The editor is expected to provide some way of scrolling the text if
it is too long. However, it may provide a fixed size preview first,
where  the user has to explicitly request the full description to
get the scrolling. This often means clicking on the preview, but there
are exceptions; in `emacs`, the user would run `M-X eldoc-doc-buffer`.

The editor does not inform the `debputy` program whether a fixed
size preview or the full text is rendered. It is the same request
(and the same string) for both cases and often done in a single
request. Nevertheless, it does mean that it can be worth squeezing as
much juice as possible out of the first few lines, as it can sometimes
enable the user to move on faster.

There is no rule for how large such a fixed preview is or how many
lines it spans. It depends on the editor, the font and window
size the editor is subject to at the time, etc. Therefore, there
is no hard limit on what is considered "the first few lines".

Note that `debputy` does not provide its own Markdown-to-text
module. If the editor does not support Markdown, the raw Markdown
is returned as the hover doc tagged as "plaintext" documentation.
This means the raw markdown must be able to stand alone without
being confusing to the reader.

TODO:

 * The headers themselves are currently not translatable.


# Testing / Validating the strings

Currently, the best way to test the strings is to use the `debputy`
LSP module in your editor and try out how it works in practice.

This is a bit involved and requires that you have a code editor that supports LSP
(see <https://microsoft.github.io/language-server-protocol/implementors/tools/>
for a list of editors that are known to support this). The guide below
assumes you are working in a git checkout of the `debputy` source code.

If you are interested in this, please:

    1) Install `debputy-lsp` or `debputy`.

    2) Run `./debputy.sh lsp features`

Which will tell you if you are missing any packages for the LSP
module to run. On success, you should see `minimum requirements: enabled`
under a `General features:` header.

Then place your translation file into this directory (po/debputy-lsp-data)
named after the translation code (such as `da.po` or `en_GB.po`),

From there, you need to add the relevant editor configuration glue using
`./debputy.sh lsp server --force-locale=XX` or
`/path/to/debputy.sh lsp server --force-locale=XX` and use your editor on
a relevant file from there. The use of `debputy.sh` is to ensure you
are using. The `--force-locale XX` parameter is optional but ensures
`debputy` is using the specific language instead of the default. Otherwise,
the editor or the environment might cause `debputy` to pick up another
localization code.

# Improving the generated POT file itself

If you find yourself wanting to improve the POT itself, then please
review the [update_lsp_data_pots.py](../../devutils/update_lsp_data_pots.py)
script.
