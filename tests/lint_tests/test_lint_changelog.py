import textwrap

import pytest

from debputy.lsp.lsp_debian_changelog import _lint_debian_changelog
from debputy.lsprotocol.types import DiagnosticSeverity
from debputy.packages import DctrlParser
from debputy.plugin.api.feature_set import PluginProvidedFeatureSet
from lint_tests.lint_tutil import LintWrapper, diag_range_to_text


@pytest.fixture
def line_linter(
    debputy_plugin_feature_set: PluginProvidedFeatureSet,
    lint_dctrl_parser: DctrlParser,
) -> LintWrapper:
    return LintWrapper(
        "/nowhere/debian/changelog",
        _lint_debian_changelog,
        debputy_plugin_feature_set,
        lint_dctrl_parser,
    )


def test_dch_lint(line_linter: LintWrapper) -> None:
    lines = textwrap.dedent(
        """\
    foo (0.2) unstable; urgency=medium

      * Renamed to foo
    
     -- Niels Thykier <niels@thykier.net>  Mon, 08 Apr 2024 16:00:00 +0000

    bar (0.2) unstable; urgency=medium

      * Initial release
    
     -- Niels Thykier <niels@thykier.net>  Mon, 01 Apr 2024 00:00:00 +0000
    """
    ).splitlines(keepends=True)

    diagnostics = line_linter(lines)
    print(diagnostics)
    # Without a control file, this is fine
    assert not diagnostics

    line_linter.dctrl_lines = textwrap.dedent(
        """\
    Source: foo
    
    Package: something-else
    """
    ).splitlines(keepends=True)

    diagnostics = line_linter(lines)
    print(diagnostics)
    # Also fine, because d/control and d/changelog agrees
    assert not diagnostics

    line_linter.dctrl_lines = textwrap.dedent(
        """\
    Source: bar

    Package: something-else
    """
    ).splitlines(keepends=True)

    diagnostics = line_linter(lines)
    print(diagnostics)
    # This should be problematic though
    assert diagnostics and len(diagnostics) == 1
    diag = diagnostics[0]

    msg = (
        "The first entry must use the same source name as debian/control."
        ' Changelog uses: "foo" while d/control uses: "bar"'
    )
    assert diag.severity == DiagnosticSeverity.Error
    assert diag.message == msg
    assert f"{diag.range}" == "0:0-0:3"


def test_dch_lint_historical(line_linter: LintWrapper) -> None:
    nonsense = "very very very very very very very very very very very very very very "
    lines = textwrap.dedent(
        f"""\
    foo (0.4) unstable; urgency=medium

      * A {nonsense} long line about absolute nothing that should trigger a warning about length.

     -- Niels Thykier <niels@thykier.net>  Mon, 08 Apr 2024 16:00:00 +0000

    foo (0.3) unstable; urgency=medium

      * Another entry that is not too long.

     -- Niels Thykier <niels@thykier.net>  Thu, 04 Apr 2024 00:00:00 +0000

    foo (0.2) unstable; urgency=medium

      * A {nonsense}  long line about absolute nothing that should not trigger a warning about length.

     -- Niels Thykier <niels@thykier.net>  Mon, 01 Apr 2024 00:00:00 +0000
    """
    ).splitlines(keepends=True)
    diagnostics = line_linter(lines)
    print(diagnostics)
    # This should be problematic though
    assert diagnostics and len(diagnostics) == 1
    diag = diagnostics[0]

    msg = "Line exceeds 82 characters"
    assert diag.severity == DiagnosticSeverity.Hint
    assert diag.message == msg
    assert f"{diag.range}" == "2:82-2:153"


def test_dch_lint_invalid_version(line_linter: LintWrapper) -> None:
    lines = textwrap.dedent(
        f"""\
    foo (a!0.3!a) unstable; urgency=medium

      * Another entry that is not too long.

     -- Niels Thykier <niels@thykier.net>  Thu, 04 Apr 2024 00:00:00 +0000

    foo (a+!) unstable; urgency=medium

      * Initial release

     -- Niels Thykier <niels@thykier.net>  Mon, 01 Apr 2024 00:00:00 +0000
    """
    ).splitlines(keepends=True)
    diagnostics = line_linter(lines)
    print(diagnostics)
    assert diagnostics and len(diagnostics) == 3
    first_issue, second_issue, third_issue = diagnostics

    msg = "This part cannot be parsed as a valid Debian version."
    assert first_issue.severity == DiagnosticSeverity.Error
    assert first_issue.message == msg
    assert f"{first_issue.range}" == "0:5-0:7"

    msg = "This part cannot be parsed as a valid Debian version."
    assert second_issue.severity == DiagnosticSeverity.Error
    assert second_issue.message == msg
    assert f"{second_issue.range}" == "0:10-0:12"

    msg = 'Cannot parse "a+!" as a Debian version.'
    assert third_issue.severity == DiagnosticSeverity.Error
    assert third_issue.message == msg
    assert f"{third_issue.range}" == "6:5-6:8"


def test_dch_lint_version_typo_dfsg(line_linter: LintWrapper) -> None:
    lines = textwrap.dedent(
        f"""\
    foo (0.3+dsfg) unstable; urgency=medium

      * Another entry that is not too long.

     -- Niels Thykier <niels@thykier.net>  Thu, 04 Apr 2024 00:00:00 +0000

    foo (0.2) unstable; urgency=medium

      * Initial release

     -- Niels Thykier <niels@thykier.net>  Mon, 01 Apr 2024 00:00:00 +0000
    """
    ).splitlines(keepends=True)
    diagnostics = line_linter(lines)
    print(diagnostics)
    assert diagnostics and len(diagnostics) == 1
    issue = diagnostics[0]

    msg = 'Typo of "dfsg" (Debian Free Software Guidelines)'
    assert issue.severity == DiagnosticSeverity.Hint
    assert issue.message == msg
    assert f"{issue.range}" == "0:9-0:13"
    assert diag_range_to_text(lines, issue.range) == "dsfg"
