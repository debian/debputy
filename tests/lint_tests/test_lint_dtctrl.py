import textwrap

import pytest

from debputy.lsp.lsp_debian_tests_control import _lint_debian_tests_control
from debputy.packages import DctrlParser
from debputy.plugin.api.feature_set import PluginProvidedFeatureSet
from lint_tests.lint_tutil import (
    requires_levenshtein,
    LintWrapper,
)
from debputy.lsprotocol.types import DiagnosticSeverity


@pytest.fixture
def line_linter(
    debputy_plugin_feature_set: PluginProvidedFeatureSet,
    lint_dctrl_parser: DctrlParser,
) -> LintWrapper:
    return LintWrapper(
        "/nowhere/debian/tests/control",
        _lint_debian_tests_control,
        debputy_plugin_feature_set,
        lint_dctrl_parser,
    )


def test_dtctrl_lint_tests_vs_test_command(line_linter: LintWrapper) -> None:
    lines = textwrap.dedent(
        """\
    Depends:
     @,
     python3-all,
     python3-pytest,
     python3-pytest-mock,
     python3-pytest-xvfb,
     xauth,
     xvfb,
    Restrictions: allow-stderr

    Tests: foo
    Test-Command: bar
    Depends:
     @,
     python3-all,
     python3-pytest,
     python3-pytest-mock,
     python3-pytest-xvfb,
     xauth,
     xvfb,
    Restrictions: allow-stderr
    """
    ).splitlines(keepends=True)

    diagnostics = line_linter(lines)
    print(diagnostics)
    assert len(diagnostics) == 2
    first_error, second_error = diagnostics

    msg = 'Stanza must have either a "Tests" or a "Test-Command" field'
    assert first_error.message == msg
    assert f"{first_error.range}" == "0:0-8:0"
    assert first_error.severity == DiagnosticSeverity.Error

    msg = 'Stanza cannot have both a "Tests" and a "Test-Command" field'
    assert second_error.message == msg
    assert f"{second_error.range}" == "10:0-11:0"
    assert second_error.severity == DiagnosticSeverity.Error


@requires_levenshtein
def test_dtctrl_lint_live_example_silx(line_linter: LintWrapper) -> None:
    lines = textwrap.dedent(
        """\
    Tests: no-opencl
    Depends:
     @,
     python3-all,
     python3-pytest,
     python3-pytest-mock,
     python3-pytest-xvfb,
     xauth,
     xvfb,
    Restrictions: allow-stderr

    Tests: opencl
    Depends:
     @,
     clinfo,
     python3-all,
     python3-pytest,
     python3-pytest-mock,
     python3-pytest-xvfb,
     xauth,
     xvfb,
    Architecture: !i386
    Restrictions: allow-stderr

    Test-Command: xvfb-run -s "-screen 0 1024x768x24 -ac +extension GLX +render -noreset" sh debian/tests/gui
    Depends:
     mesa-utils,
     silx,
     xauth,
     xvfb,
    Restrictions: allow-stderr
    """
    ).splitlines(keepends=True)

    diagnostics = line_linter(lines)
    print(diagnostics)
    assert not diagnostics
