import textwrap

import pytest

from debputy.lsp.lsp_debian_upstream_metadata import _lint_debian_upstream_metadata
from debputy.packages import DctrlParser
from debputy.plugin.api.feature_set import PluginProvidedFeatureSet
from lint_tests.lint_tutil import (
    LintWrapper,
)

from debputy.lsprotocol.types import DiagnosticSeverity


@pytest.fixture
def line_linter(
    debputy_plugin_feature_set: PluginProvidedFeatureSet,
    lint_dctrl_parser: DctrlParser,
) -> LintWrapper:
    return LintWrapper(
        "/nowhere/debian/upstream/metadata",
        _lint_debian_upstream_metadata,
        debputy_plugin_feature_set,
        lint_dctrl_parser,
    )


def test_metadata_lint_unknown_keys(line_linter: LintWrapper) -> None:
    lines = textwrap.dedent(
        """\
    Bar: bar
    """
    ).splitlines(keepends=True)

    diagnostics = line_linter(lines)

    print(diagnostics)
    assert len(diagnostics) == 1
    error = diagnostics[0]
    msg = 'Unknown or unsupported key "Bar".'
    assert error.message == msg
    assert error.severity == DiagnosticSeverity.Warning
    assert f"{error.range}" == "0:0-0:3"
